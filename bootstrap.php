<?php
/**
 * bootstrap.php
 *
 * @author: chazer
 * @created: 23.09.14 23:17
 */

require_once(__DIR__."/lib/SplEnum.php");

$schemesPath = dirname(__FILE__) . '/src/api/specs/';

foreach (glob($schemesPath.'*.php') as $file) {
    require_once($file);
}

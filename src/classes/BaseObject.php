<?php
/**
 * BaseObject.php
 *
 * @author: chazer
 * @created: 23.09.14 14:04
 */

namespace chazer\amocrmlib\classes;

class BaseObject
{
    public static function getClassName()
    {
        return get_called_class();
    }
} 
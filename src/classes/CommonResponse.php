<?php
/**
 * CommonResponse.php
 *
 * @author: chazer
 * @created: 30.09.14 16:02
 */

namespace chazer\amocrmlib\classes;

use chazer\amocrmlib\api\interfaces\IResponse;
use chazer\amocrmlib\api\interfaces\IRequest;

class CommonResponse implements IResponse
{
    /** @var AccountInfo */
    protected $accInfo;
    protected $data;

    /** @var IRequest */
    protected $reqObj;

    protected $server_time;

    public function __construct($accInfo = null, $data = null)
    {
        if ($accInfo) {
            $this->setAccountInfo($accInfo);
        }
        if ($data) {
            $this->setData($data);
        }
    }

    /**
     * @param $accInfo
     */
    public function setAccountInfo($accInfo)
    {
        $this->accInfo = $accInfo;
    }

    /**
     * @param IRequest $request
     * @return void
     */
    public function setRequestObject($request)
    {
        $this->reqObj = $request;
    }

    /**
     * @param Array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    public function getServerTime()
    {
        return $this->data['server_time'];
    }
} 
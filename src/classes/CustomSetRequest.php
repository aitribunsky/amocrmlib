<?php
/**
 * CustomSetRequest.php
 *
 * @author: chazer
 * @created: 30.09.14 12:53
 */

namespace chazer\amocrmlib\classes;

use chazer\amocrmlib\api\interfaces\ICommonListAddItem;
use chazer\amocrmlib\api\interfaces\ICommonListUpdateItem;
use chazer\amocrmlib\api\interfaces\ISetRequest;
use chazer\amocrmlib\Api;

class CustomSetRequest extends SchemeObject implements ISetRequest
{
    protected $listAdd = [];
    protected $listUpdate = [];

    /**
     * @param Api $api
     * @return bool|ResponseSetCommand
     */
    public function send(Api $api)
    {
        return $api->send($this);
    }

    /**
     * @return ICommonListAddItem[]
     */
    public function getAdd()
    {
        return $this->listAdd;
    }

    /**
     * @return ICommonListUpdateItem[]
     */
    public function getUpdate()
    {
        return $this->listUpdate;
    }
} 
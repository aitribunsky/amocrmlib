<?php
/**
 * ListUpdateItem.php
 *
 * @author: chazer
 * @created: 25.09.14 14:44
 */

namespace chazer\amocrmlib\classes\contacts;

use chazer\amocrmlib\api\interfaces\fields\ICustomField;
use chazer\amocrmlib\api\interfaces\contacts\IListUpdateItem;
use chazer\amocrmlib\classes\SchemeObject;
use DateTime;

class ListUpdateItem extends SchemeObject implements IListUpdateItem
{
    /**
     * @var int Уникальный идентификатор контакта, который указывается с целью его обновления
     */
    public $id;

    /**
     * @var string [require] Имя контакта
     */
    public $name;

    /**
     * @var int Уникальный идентификатор записи в клиентской программе (не обязательный параметр)
     */
    public $request_id;

    /**
     * @var DateTime Дата создания этого контакта (не обязательный параметр)
     */
    public $date_create;

    /**
     * @var DateTime [require] Дата последнего изменения этого контакта
     */
    public $last_modified;

    /**
     * @var int Уникальный идентификатор ответственного пользователя (пользователи см. Информация аккаунта -
     * https://developers.amocrm.ru/rest_api/accounts_current.php )
     */
    public $responsible_user_id;

    /**
     * @var array Список id связанных сделок
     */
    public $linked_leads_id;

    /**
     * @var string Имя компании
     */
    public $company_name;

    /**
     * @var array Дополнительные поля контакта
     */
    public $custom_fields;

    /**
     * @var string Названия тегов через запятую
     */
    public $tags;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getRequestId()
    {
        return $this->request_id;
    }

    public function getDateCreate()
    {
        return $this->date_create;
    }

    public function getLastModified()
    {
        return $this->last_modified;
    }

    public function getResponsibleUserId()
    {
        return $this->responsible_user_id;
    }

    public function linkLead($id)
    {
        if (!is_array($this->linked_leads_id))
            $this->linked_leads_id = [];
        $this->linked_leads_id[] = $id;
    }

    public function getLinkedLeadsId()
    {
        return $this->linked_leads_id;
    }

    public function getCompanyName()
    {
        return $this->company_name;
    }

    public function addCustomField(ICustomField $item)
    {
        if (!is_array($this->custom_fields))
            $this->custom_fields = [];
        $this->custom_fields[] = $item;
    }

    public function getCustomFields()
    {
        return $this->custom_fields;
    }

    public function getTags()
    {
        return $this->tags;
    }
}

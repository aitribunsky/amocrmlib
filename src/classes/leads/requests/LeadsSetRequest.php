<?php
/**
 * LeadsSetRequest.php
 *
 * @author: chazer
 * @created: 26.09.14 20:52
 */

namespace chazer\amocrmlib\classes\leads\requests;

use chazer\amocrmlib\api\interfaces\leads\IListAddItem;
use chazer\amocrmlib\api\interfaces\leads\IListUpdateItem;
use chazer\amocrmlib\api\interfaces\leads\requests\ILeadsSetRequest;
use chazer\amocrmlib\classes\CustomSetRequest;
use chazer\amocrmlib\classes\leads\ListAddItem;
use chazer\amocrmlib\classes\leads\ListUpdateItem;
use chazer\amocrmlib\classes\Schemes;

/**
 * Class LeadsSetRequest
 * @method IListAddItem[] getAdd
 * @method IListUpdateItem[] getUpdate
 * @package chazer\amocrmlib\classes\leads\requests
 */
class LeadsSetRequest extends CustomSetRequest implements ILeadsSetRequest
{
    public function add(IListAddItem $item)
    {
        Schemes::validateObject($item, ListAddItem::getSchemeName());
        $this->listAdd[] = $item;
    }

    public function update(IListUpdateItem $item)
    {
        Schemes::validateObject($item, ListUpdateItem::getSchemeName());
        $this->listUpdate[] = $item;
    }
} 
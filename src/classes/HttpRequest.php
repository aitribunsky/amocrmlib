<?php
/**
 * HttpRequest.php
 *
 * @author: chazer
 * @created: 23.09.14 14:14
 */

namespace chazer\amocrmlib\classes;

use chazer\amocrmlib\interfaces\ICurl;
use chazer\amocrmlib\interfaces\IHttpRequest;

class HttpRequest implements ICurl, IHttpRequest
{
    private $handle;

    public $debug = 0;
    private $inited = false;
    private $errors = [];

    protected $paramUserAgent = 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)';
    protected $defaultHeaders = [
    ];
    protected $paramConnectTimeout = 5; // seconds
    protected $paramExecuteTimeout = 15; // seconds
    protected $keepCookies = false;
    protected $followLocation = false;
    protected $cookies = [];
    protected $savedCookies = [];

    protected $responseCode;
    protected $responseBody;
    protected $responseContentType;
    protected $rawResponseHeaders;
    protected $responseHeaders;

    protected $rawRequestHeaders;
    protected $requestHeaders;

    public function curlInit($url = null)
    {
        $this->handle = curl_init($url);
    }

    public function curlSetOpt($option, $value)
    {
        return curl_setopt($this->handle, $option, $value);
    }

    public function curlGetInfo($opt = null)
    {
        return curl_getinfo($this->handle, $opt);
    }

    public function curlExec()
    {
        return curl_exec($this->handle);
    }

    public function curlError()
    {
        return curl_error($this->handle);
    }

    public function curlErrNo()
    {
        return curl_errno($this->handle);
    }

    public function curlClose()
    {
        curl_close($this->handle);
    }

    public function reset()
    {
        $this->cookie = '';
        if ($this->handle)
            $this->curlClose();
        $this->handle = null;
    }

    protected function init()
    {
        if (!$this->inited) {
            $this->curlInit();
            $this->inited = true;
            $this->curlSetOpt(CURLOPT_USERAGENT, $this->paramUserAgent);
            $this->curlSetOpt(CURLOPT_CONNECTTIMEOUT, $this->paramConnectTimeout);
            $this->curlSetOpt(CURLOPT_TIMEOUT, $this->paramExecuteTimeout);
            $this->curlSetOpt(CURLOPT_FOLLOWLOCATION, false);
            $this->curlSetOpt(CURLOPT_RETURNTRANSFER, true);
            $this->curlSetOpt(CURLOPT_SSL_VERIFYPEER, false);
            $this->curlSetOpt(CURLOPT_SSL_VERIFYHOST, false);
            //$this->setOpt(CURLOPT_FORBID_REUSE, FALSE);
        }
    }

    public function getUserAgent()
    {
        return $this->paramUserAgent;
    }

    public function setUserAgent($value)
    {
        $this->init();
        if ($this->paramUserAgent != $value) {
            $this->paramUserAgent = $value;
            $this->curlSetOpt(CURLOPT_USERAGENT, $this->paramUserAgent);
        }
    }

    public function setTimeoutConnect($value)
    {
        $this->init();
        if ($this->paramConnectTimeout != $value) {
            $this->paramConnectTimeout = $value;
            $this->curlSetOpt(CURLOPT_CONNECTTIMEOUT, $this->paramConnectTimeout);
        }
    }

    public function setTimeoutExecute($value)
    {
        $this->init();
        if ($this->paramExecuteTimeout != $value) {
            $this->paramExecuteTimeout = $value;
            $this->curlSetOpt(CURLOPT_TIMEOUT, $this->paramExecuteTimeout);
        }
    }

    public function enableFollowLocation($value)
    {
        $this->followLocation = $value;
    }

    public function enableKeepCookies($enable)
    {
        $this->keepCookies = $enable;
    }

    protected function formatCookies($cookies)
    {
        return implode('; ',
            array_map(function ($v, $k) { return $k . '=' . $v; }, $cookies, array_keys($cookies))
        );
    }

    protected function beforeRequest()
    {
        $this->errors = [];
        $this->responseBody = null;
        $this->responseCode = null;
        $this->responseHeaders = [];
        $this->rawResponseHeaders = null;
        $this->rawRequestHeaders = null;
        if (!$this->keepCookies) {
            $this->savedCookies = [];
        }
        $this->cookies = $this->getCookies();
    }

    private function parseCookies($rawResponseHeaders)
    {
        if (preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $rawResponseHeaders, $matches)) {
            $newCookies = [];
            foreach($matches[1] as $line) {
                $data = [];
                parse_str($line, $data);
                if ($data) {
                    $newCookies = array_merge($newCookies, $data);
                }
            }
            if ($this->debug >= 3) {
                echo "NEW COOKIES: " . $this->formatCookies($newCookies) . PHP_EOL;
            }
            $this->savedCookies = array_merge($this->getCookies(), $newCookies);
        }
    }

    private function parseLocation($rawResponseHeaders)
    {
        if (preg_match("/^Location:\\s*([^\r\n]*)/mi", $rawResponseHeaders, $matches)) {
            return $matches[1];
        }
        return false;
    }

    private function setCurlHeaders($headers)
    {
        if (!empty($headers)) {
            $list = [];
            foreach($headers as $k=>$v)
                $list[] = $k.': '.$v;
            if ($this->debug >= 3) {
                echo "SEND HEADERS: " . var_export($list, true) . PHP_EOL;
            }
            $this->curlSetOpt(CURLOPT_HTTPHEADER, $list);
        }
    }

    private function setCurlCookies()
    {
        if (!empty($this->cookies)) {
            $str = $this->formatCookies($this->cookies);
            if ($this->debug >= 3) {
                echo "SEND COOKIES: " . $str . PHP_EOL;
            }
            $this->curlSetOpt(CURLOPT_COOKIE, $str);
        }
    }

    private function setCurlMethod($method)
    {
        if ($method == 'GET') {
            $this->curlSetOpt(CURLOPT_HTTPGET, true);
        }
        if ($method == 'POST') {
            $this->curlSetOpt(CURLOPT_POST, true);
            $this->curlSetOpt(CURLOPT_POSTFIELDS, null);
        }
        if ($method == 'DELETE') {
            $this->curlSetOpt(CURLOPT_CUSTOMREQUEST, $method);
        }
        if ($method == 'PUT') {
            $this->curlSetOpt(CURLOPT_CUSTOMREQUEST, $method);
        }
    }

    public function query($url, $method = 'GET', $post = null, $headers = null)
    {
        $this->init();

        $this->curlSetOpt(CURLOPT_URL, $url);
        $this->curlSetOpt(CURLINFO_HEADER_OUT, true); // отслеживание строки запроса.
        $this->curlSetOpt(CURLOPT_HEADER, true); // включение заголовков в вывод.
//        $this->curlSetOpt(CURLOPT_CRLF, false);
//        $this->curlSetOpt(CURLOPT_BINARYTRANSFER, true) ;

        $this->beforeRequest();

        $this->setCurlMethod($method);

        $headers = array_merge($this->getDefaultHeaders(), $headers ? $headers : []);

        if (!empty($post)) {
            $this->curlSetOpt(CURLOPT_POSTFIELDS, $post);
            //$this->curlSetOpt(CURLOPT_HTTPHEADER, array('Expect:'));
            $headers['Expect'] = '';
        }

        $this->setCurlHeaders($headers);

        do {
            $this->setCurlCookies();

            $rawResponse = $this->curlExec();
            $respError = $this->curlError();

            if ($respError) {
                $this->addErrors($respError);
            }
            if (false === $rawResponse) {
                return false;
            }

            $this->responseCode = $this->curlGetInfo(CURLINFO_HTTP_CODE);
            $headerSize = $this->curlGetInfo(CURLINFO_HEADER_SIZE);
            $this->responseContentType = $this->curlGetInfo(CURLINFO_CONTENT_TYPE);
            $rawResponseHeaders = substr($rawResponse, 0, $headerSize);

            $this->parseCookies($rawResponseHeaders);

            if ($this->debug >= 4) {
                echo "REQUEST: " . PHP_EOL . ($this->curlGetInfo(CURLINFO_HEADER_OUT)) . PHP_EOL;
                echo "RESPONSE: " . PHP_EOL . ($rawResponseHeaders) . PHP_EOL;
            }

            $repeat = false;
            if ($this->followLocation &&
                in_array($this->responseCode, [301, 302]) &&
                ($redirectUrl = $this->parseLocation($rawResponseHeaders))
            ) {
                if ($this->debug >= 3) {
                    echo "REDIRECT: " . $redirectUrl . PHP_EOL;
                }
                $this->curlSetOpt(CURLOPT_URL, $redirectUrl);
                $repeat = true;
            }
        } while ($repeat);

        $this->responseBody = strlen($rawResponse) > $headerSize ? substr($rawResponse, $headerSize) : '';

        //echo "REQUEST: " . (curl_getinfo($curl, CURLINFO_HEADER_OUT)) . "\r\n";
        //echo "RESPONSE: " . ($res_headers) . "\r\n";
        //echo "RESPONSE BODY: " . ($respBody) . "\r\n";

        $this->rawResponseHeaders = $rawResponseHeaders;

        $this->rawRequestHeaders = $this->curlGetInfo(CURLINFO_HEADER_OUT);;

        return true;
    }

    public function getResponseCode()
    {
        return $this->responseCode;
    }

    public function getResponseBody()
    {
        return $this->responseBody;
    }

    public function getResponseHeaders()
    {
        if (empty($this->responseHeaders)) {
            $this->responseHeaders = [];
            if ($data = explode("\n", $this->rawResponseHeaders)) {
                array_shift($data);
                $headers = [];
                foreach($data as $part){
                    $h = explode(": ",$part, 2);
                    if ($h[0] = trim($h[0])){
                        $headers[$h[0]] = trim($h[1]);
                    }
                }
                $this->responseHeaders = $headers;
            }
        }
        return $this->responseHeaders;
    }

    public function getRequestHeaders()
    {
        if (empty($this->requestHeaders)) {
            $data = explode("\n", $this->rawRequestHeaders);
            array_shift($data);
            $headers = [];
            foreach($data as $part){
                $h = explode(": ",$part, 2);
                if ($h[0] = trim($h[0])){
                    $headers[$h[0]] = trim($h[1]);
                }
            }
            $this->requestHeaders = $headers;
        }
        return $this->requestHeaders;
    }

    public function getCookies()
    {
        $a = $this->cookies;
        $b = $this->savedCookies;
        return array_merge(is_array($a) ? $a : [], is_array($b) ? $b : []);
    }

    public function setCookies($array)
    {
        $this->cookies = $array;
        $this->savedCookies = [];
    }

    public function getResponseContentType()
    {
        return $this->responseContentType;
    }

    public function getDefaultHeaders()
    {
        return $this->defaultHeaders;
    }

    public function setDefaultHeaders($array, $replace = false)
    {
        $this->defaultHeaders = $replace ? $array : array_merge($this->defaultHeaders, $array);
    }

    public function hasErrors()
    {
        return !empty($this->errors);
    }

    public function getErrors()
    {
        return $this->errors;
    }

    protected function addErrors($message)
    {
        $this->errors[] = $message;
    }
} 
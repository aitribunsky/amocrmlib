<?php
/**
 * ResponseSetCommand.php
 *
 * @author: chazer
 * @created: 29.09.14 12:47
 */

namespace chazer\amocrmlib\classes;

use chazer\amocrmlib\api\interfaces\ISetResponse;
use chazer\amocrmlib\api\interfaces\ISetRequest;

/**
 * Class ResponseSetCommand
 * @package chazer\amocrmlib\classes
 */
class ResponseSetCommand extends CommonResponse implements ISetResponse
{
    /** @var EntityId[] */
    protected $listAddItems;
    protected $listUpdateItems;
    protected $updateItems;

    /** @var ISetRequest */
    protected $reqObj;

    /** @param ISetRequest $request */
    public function setRequestObject($request)
    {
        parent::setRequestObject($request);
    }

    public function hasErrors()
    {
        $a = count($this->reqObj->getAdd());
        $b = count($this->getAddedList());
        $c = count($this->getNotUpdatedList());
        return ($a != $b) || ($c > 0);
    }

    public function getAddedList()
    {
        if (is_null($this->listAddItems)) {
            $out = [];
            if (isset($this->data['add']) && !empty($this->data['add'])) {
                foreach($this->data['add'] as $data) {
                    $out[] = new EntityId(array_merge($data, [
                        'server_time' => $this->getServerTime(),
                    ]));
                }
            }
            $this->listAddItems = $out;
        }
        return $this->listAddItems;
    }

    public function getNotUpdatedList()
    {
        if (is_null($this->listUpdateItems)) {
            $out = [];
            if (isset($this->data['update']) && !empty($this->data['update'])) {
                $out = $this->data['update'];
            }
            $this->listUpdateItems = $out;
        }
        return $this->listUpdateItems;
    }
}
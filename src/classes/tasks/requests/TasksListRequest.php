<?php
/**
 * TasksListRequest.php
 *
 * @author: chazer
 * @created: 23.09.14 13:31
 */

namespace chazer\amocrmlib\classes\tasks\requests;

use chazer\amocrmlib\api\interfaces\tasks\requests\ITasksListRequest;
use chazer\amocrmlib\classes\SchemeObject;

class TasksListRequest extends SchemeObject implements ITasksListRequest
{
    /** @var null|int timestamp */
    public $ifModifiedSince;

    /** @var null|string contact or lead
    Получение данных только для контакта или сделки
    при отсутствии этого параметра будут полученны все данные, в том числе не прикрепленные к объектам */
    public $type;

    /** @var null|int Кол-во выбираемых строк (системное ограничение 500) */
    public $limit_rows;

    /** @var null|int Оффсет выборки (с какой строки выбирать) Работает, только при условии, что limit_rows тоже указан */
    public $limit_offset;

    /** @var null|array|int Выбрать элемент с заданным ID (Если указан этот параметр, все остальные игнорируются)
     * (Можно передавать в виде массива состоящий из нескольких ID)*/
    public $id;

    /** @var null|array|int Дополнительный фильтр поиска, по ответственному пользователю
     * (Можно передавать в виде массива) */
    public $responsible_user_id;

    public function getIfModifiedSince()
    {
        return $this->ifModifiedSince;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getLimitRows()
    {
        return $this->limit_rows;
    }

    public function getLimitOffset()
    {
        return $this->limit_offset;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getResponsibleUserId()
    {
        return $this->responsible_user_id;
    }

}

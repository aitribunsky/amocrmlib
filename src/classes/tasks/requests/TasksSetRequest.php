<?php
/**
 * TasksSetRequest.php
 *
 * @author: chazer
 * @created: 22.09.14 23:51
 */

namespace chazer\amocrmlib\classes\tasks\requests;

use chazer\amocrmlib\api\interfaces\tasks\IListAddItem;
use chazer\amocrmlib\api\interfaces\tasks\IListUpdateItem;
use chazer\amocrmlib\api\interfaces\tasks\requests\ITasksSetRequest;
use chazer\amocrmlib\classes\CustomSetRequest;
use chazer\amocrmlib\classes\Schemes;
use chazer\amocrmlib\classes\tasks\ListAddItem;
use chazer\amocrmlib\classes\tasks\ListUpdateItem;

/**
 * Class TasksSetRequest
 * @method IListAddItem[] getAdd
 * @method IListUpdateItem[] getUpdate
 * @package chazer\amocrmlib\classes\tasks\requests
 */
class TasksSetRequest extends CustomSetRequest implements ITasksSetRequest
{
    public function add(IListAddItem $item)
    {
        Schemes::validateObject($item, ListAddItem::getSchemeName());
        $this->listAdd[] = $item;
    }

    public function update(IListUpdateItem $item)
    {
        Schemes::validateObject($item, ListUpdateItem::getSchemeName());
        $this->listUpdate[] = $item;
    }
}

<?php
/**
 * SchemeObject.php
 *
 * @author: chazer
 * @created: 23.09.14 16:24
 */

namespace chazer\amocrmlib\classes;

class SchemeObject extends BaseObject
{
    public static function getSchemeName($class = null)
    {
        $class = $class ? $class : get_called_class();
        $list = class_implements($class, false);
        $reflect = new \ReflectionClass($class);
        $name = $reflect->getShortName();
        //$ifaces = $reflect->getInterfaceNames();
        $l = strlen($name);
        foreach($list as $i) {
            if (strlen($i) - $l == strrpos($i, $name)) {
                return $i;
            }
        }
        throw new \Exception('Scheme not detected');
    }
} 
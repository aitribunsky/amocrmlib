<?php
/**
 * ListAddItem.php
 *
 * @author: aitribunsky
 * @created: 13.02.15 20:29
 */
namespace chazer\amocrmlib\classes\notes;

use chazer\amocrmlib\api\interfaces\notes\IListAddItem;
use chazer\amocrmlib\classes\SchemeObject;

class ListAddItem extends SchemeObject implements IListAddItem
{

    public $element_id;

    public $element_type;

    public $note_type;

    public $date_create;

    public $request_id;

    public $last_modified;

    public $text;

    public $responsible_user_id;

    public function getElementId()
    {
        return $this->element_id;
    }

    public function getElementType()
    {
        return $this->element_type;
    }

    public function getNoteType()
    {
        return $this->note_type;
    }

    public function getDateCreate()
    {
        return $this->date_create;
    }

    public function getRequestId()
    {
        return $this->request_id;
    }

    public function getLastModified()
    {
        return $this->last_modified;
    }

    public function getText()
    {
        return $this->text;
    }

    public function getResponsibleUserId()
    {
        return $this->responsible_user_id;
    }
}
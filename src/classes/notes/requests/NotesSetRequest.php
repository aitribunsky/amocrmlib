<?php
/**
 * NotesSetRequest.php
 *
 * @author: aitribunsky
 * @created: 13.02.15 20:52
 */
namespace chazer\amocrmlib\classes\notes\requests;

use chazer\amocrmlib\api\interfaces\notes\IListAddItem;
use chazer\amocrmlib\api\interfaces\notes\IListUpdateItem;
use chazer\amocrmlib\api\interfaces\notes\requests\INotesSetRequest;
use chazer\amocrmlib\classes\CustomSetRequest;
use chazer\amocrmlib\classes\notes\ListAddItem;
use chazer\amocrmlib\classes\notes\ListUpdateItem;
use chazer\amocrmlib\classes\Schemes;

/**
 * Class notesSetRequest
 *
 * @method IListAddItem[] getAdd
 * @method IListUpdateItem[] getUpdate
 * @package chazer\amocrmlib\classes\notes\requests
 */
class NotesSetRequest extends CustomSetRequest implements INotesSetRequest
{

    public function add(IListAddItem $item)
    {
        Schemes::validateObject($item, ListAddItem::getSchemeName());
        $this->listAdd[] = $item;
    }

    public function update(IListUpdateItem $item)
    {
        Schemes::validateObject($item, ListUpdateItem::getSchemeName());
        $this->listUpdate[] = $item;
    }
} 
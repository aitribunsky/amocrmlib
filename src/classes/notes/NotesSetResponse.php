<?php
/**
 * NotesSetResponse.php
 *
 * @author: aitribunsky
 * @created: 13.02.15 1:55
 */
namespace chazer\amocrmlib\classes\notes;

class NotesSetResponse
{

    /**
     *
     * @var int Уникальный идентификатор новой сущности
     */
    public $id;

    /**
     *
     * @var int Уникальный идентификатор сущности в клиентской программе, если request_id не передан в запросе,
     *      то он генерируется автоматически
     */
    public $request_id;

    public function getId()
    {
        return $this->id;
    }

    public function getRequestId()
    {
        return $this->request_id;
    }
} 
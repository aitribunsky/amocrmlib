<?php
/**
 * CustomFieldValue.php
 *
 * @author: chazer
 * @created: 25.09.14 15:28
 */

namespace chazer\amocrmlib\classes\fields;

use chazer\amocrmlib\api\interfaces\fields\ICustomFieldValue;
use chazer\amocrmlib\classes\SchemeObject;

class CustomFieldValue extends SchemeObject implements ICustomFieldValue
{
    /** @var string Значение дополнительного поля */
    public $value;

    /** @var string Выбираемый тип дополнительного поля (напр телефон домашний, рабочий и т д) */
    public $enum;

    public function getValue()
    {
        return $this->value;
    }

    public function getEnum()
    {
        return $this->enum;
    }
} 
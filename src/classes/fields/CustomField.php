<?php
/**
 * CustomField.php
 *
 * @author: chazer
 * @created: 25.09.14 15:31
 */

namespace chazer\amocrmlib\classes\fields;

use chazer\amocrmlib\api\interfaces\fields\ICustomField;
use chazer\amocrmlib\api\interfaces\fields\ICustomFieldValue;
use chazer\amocrmlib\classes\SchemeObject;

class CustomField extends SchemeObject implements ICustomField
{
    /**
     * @var int Уникальный идентификатор заполняемого дополнительного поля (см. Информация аккаунта -
     * https://developers.amocrm.ru/rest_api/accounts_current.php)
     */
    public $id;

    /**
     * @var array Значения доп. поля
     */
    public $values;

    public function getId()
    {
        return $this->id;
    }

    public function addValue(ICustomFieldValue $item)
    {
        if (!is_array($this->values))
            $this->values = [];
        $this->values[] = $item;
    }

    public function getValues()
    {
        return $this->values;
    }
} 
<?php
/**
 * Api.php
 *
 * @author: chazer
 * @created: 23.09.14 13:50
 */

namespace chazer\amocrmlib;

use chazer\amocrmlib\api\interfaces\companies\requests\ICompaniesSetRequest;
use chazer\amocrmlib\api\interfaces\contacts\requests\IContactsSetRequest;
use chazer\amocrmlib\api\interfaces\IRequest;
use chazer\amocrmlib\api\interfaces\IResponse;
use chazer\amocrmlib\api\interfaces\leads\requests\ILeadsSetRequest;
use chazer\amocrmlib\api\interfaces\tasks\requests\ITasksListRequest;
use chazer\amocrmlib\api\interfaces\tasks\requests\ITasksSetRequest;
use chazer\amocrmlib\classes\HttpRequest;
use chazer\amocrmlib\classes\ResponseSetCommand;
use chazer\amocrmlib\classes\Schemes;
use chazer\amocrmlib\interfaces\IHttpRequest;
use chazer\amocrmlib\api\interfaces\notes\requests\INotesSetRequest;

class Api
{
    public $debug = 0;

    private $login;
    private $key;
    private $apiUri;
    private $isAuth = false;
    private $lastRequestTime = 0.0;
    private $diffServerTime = 0.0;
    private $cookies;
    private $expired;

    private $errors = [];

    private static $sessions = [];

    /** @var IHttpRequest */
    private $http;

    // HTTP status codes and messages
    static protected $_http_messages = [
        // Informational 1xx
        100 => 'Continue',
        101 => 'Switching Protocols',

        // Success 2xx
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',

        // Redirection 3xx
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found', // 1.1
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        // 306 is deprecated but reserved
        307 => 'Temporary Redirect',

        // Client Error 4xx
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',

        // Server Error 5xx
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        509 => 'Bandwidth Limit Exceeded',
    ];

    /**
     * api error codes explains
     * @var array
     * @see https://developers.amocrm.ru/rest_api/response_digest.php
     */
    static protected $_error_codes = [
        // accounts errors
        101 => 'account not found',
        102 => 'request method post but post isn\'t a valid json string',
        103 => 'request method post but post is empty',
        104 => 'api method not found',
        // system errors
        105 => 'api system error',
        106 => 'api system error',
        107 => 'api system error',
        108 => 'api system error',
        109 => 'api system error',
        // contacts errors
        201 => 'contacts add - array empty',
        202 => 'contacts add - no rights',
        203 => 'contacts add - system cf fault',
        204 => 'contacts add - cf not found',
        205 => 'contacts add - not created',
        206 => 'contacts set - request is empty',
        207 => 'contacts set - wrong request method',
        208 => 'contacts update - array empty',
        209 => 'contacts update - id and last_modified require',
        210 => 'contacts update - system cf fault',
        211 => 'contacts update - cf not found',
        212 => 'contacts update - not updated',
        // leads errors
        213 => 'leads add - array empty',
        214 => 'leads set - request is empty',
        215 => 'leads set - wrong request method',
        216 => 'leads update - array empty',
        217 => 'leads update - id,last_modified,status_id,name fields require',
        // events errors
        218 => 'notes add - array is empty',
        // system errors
        219 => 'api system error',
        220 => 'api system error',
        // events/notes errors
        221 => 'notes list - type require',
        222 => 'notes set - request is empty',
        223 => 'notes set - wrong request method',
        224 => 'notes update - array empty',
        225 => 'notes update - notes not found',
        // tasks errors
        227 => 'tasks add - array empty',
        228 => 'tasks set - request is empty',
        229 => 'tasks set - wrong request method',
        230 => 'tasks update - array empty',
        231 => 'tasks update - tasks not found',
        232 => 'notes add - element_id or element_type empty or incorrect',
        233 => 'notes add - some contacts not found by element_id',
        234 => 'notes add - some leads not found by element_id',
        235 => 'tasks add - element_type missing',
        236 => 'tasks add - some contacts not found by element_id',
        237 => 'tasks add - some leads not found by element_id',
        238 => 'contacts add - no value for cf found',
        // system errors
        239 => 'api system error',
        // leads errors
        240 => 'api system error', // Добавление/Обновление сделок: неверный параметр "id" дополнительного поля
        // system errors
        241 => 'api system error',
        242 => 'api system error',
        243 => 'api system error',
        // tasks errors
        244 => 'leads add - no rights',
        // system errors
        245 => 'contacts list - memory leak',
        // other errors
        2002 => 'no content', // Ничего не найдено. Вместе с этим ответом отдаётся HTTP код №204 "No Content"
    ];

    public function __construct($apiUri)
    {
        $this->apiUri = $apiUri;
    }

    public function setLogin($value)
    {
        if ($this->login != $value) {
            $this->login = $value;
            $this->isAuth = false;
        }
    }

    public function setAuthKey($value)
    {
        if ($this->key != $value) {
            $this->key = $value;
            $this->isAuth = false;
        }
    }

    public function setCurlObject(IHttpRequest $object)
    {
        $this->http = $object;
    }

    /**
     * @return IHttpRequest
     */
    protected function createHttp() {
        $http = new HttpRequest();
        $http->setDefaultHeaders([
            'Accept' => 'application/json, text/xml;q=0.9, application/xml;q=0.9, application/xhtml+xml;q=0.7',
            'Accept-Language' => 'ru-ru, ru;q=0.9, en-us;q=0.8, en;q=0.7',
            'Accept-Charset' => 'utf-8',
            'Connection' => 'keep-alive',
        ]);
        $http->setUserAgent('LibAmoCRM v0.0.5');
        $http->debug = $this->debug;
        return $http;
    }

    protected function getHttp()
    {
        if (!$this->http) {
            $this->http = $this->createHttp();;
        }
        return $this->http;
    }

    protected function auth()
    {
        $now = microtime(true);
        if ($this->expired <= $now)
            $this->isAuth = false;
        if ($this->isAuth)
            return;
        $this->expired = $now + 15*60;
        // Auth request here;

        $data['method'] = 'POST';
        $data['post'] = [
            'USER_LOGIN' => $this->login,
            'USER_HASH' => $this->key,
        ];
        $data['headers'] = null;
        $url = $this->buildQuery('/private/api/auth.php', ['type' => 'json']);
        $result = $this->sendRequest($url, $data['method'], $data['post'], $data['headers']);

        if ($result) {
            $x = $result['data'];
            $x = $x['response'];
            $x = $x['auth'];
            if ($x == true) {
                $this->isAuth = true;
                return;
            }
        }
        throw new \Exception('Auth error');
    }

    public function addError($message)
    {
        $this->errors[] = $message;
    }

    public function hasErrors()
    {
        return !empty($this->errors);
    }

    protected function getSchemesForRequest(IRequest $request)
    {
        return Schemes::searchSchemesForObject($request);
    }

    protected function validateRequest(IRequest $request, $schemes)
    {
        try {
            if (!$schemes) {
                throw new \Exception('No found scheme for request');
            }
            foreach ($schemes as $scheme) {
                Schemes::validateObject($request, $scheme);
            }
            $ok = true;
        } catch(\Exception $e) {
            $this->addError($e->getMessage());
            $ok = false;
        }
        return $ok;
    }

    protected function serializeRequest(IRequest $request, $schemes)
    {
        if (!$schemes) {
            throw new \Exception('No found scheme for request');
        }
        foreach ($schemes as $scheme) {
            return Schemes::serializeObject($request, $scheme);
        }
        return false;
    }

    protected function getAccountInfo()
    {
        //TODO
        return null;
    }

    public function createResponseObject(IRequest $request, $command)
    {
        switch($command){
            case 'set':
                $obj = new ResponseSetCommand();
                return $obj;
// TODO
//            case 'list':
//                $obj = new ResponseListCommand();
//                return $obj;
            default:
                $obj = null;
        }
        return false;
    }

    private function __createResponseObject(IRequest $request, $command, $data, $key)
    {
        if (method_exists($request, 'createResponseObject')) {
            $obj = $request->createResponseObject();
            if (!($obj instanceof IResponse)) {
                $obj = null;
            }
        }
        if (!isset($obj)) {
            $obj = $this->createResponseObject($request, $command);
        }
        if ($obj instanceof IResponse) {
            $obj->setAccountInfo($this->getAccountInfo());
            $obj->setRequestObject($request);
            if (isset($data[$key])) {
                $exData = $data[$key];
                unset($data[$key]);
                $data = array_merge($data, $exData);
            }
            $obj->setData($data);
            $this->updateServerTime($obj->getServerTime());
        }
        return $obj;
    }

    /**
     * @param IRequest $request
     * @return bool|string
     */
    public function buildRequest(IRequest $request)
    {
        $schemes = $this->getSchemesForRequest($request);
        if (!$this->validateRequest($request, $schemes))
            return false;

        $data = $this->serializeRequest($request, $schemes);

        $reqData = [
            'method' => 'GET',
            'entry' => '/',
            'post' => null,
            'query' => null,
            'headers' => [], // TODO: HTTP headers
            'response' => null,
        ];
        if ($request instanceof ITasksSetRequest) {
            $reqData['method'] = 'POST';
            $reqData['entry'] = '/private/api/v2/{type}/tasks/set';
            $reqData['post'] = json_encode(['request' => ['tasks' => $data]]);
            $reqData['response'] = function($data) use ($request) {
                return array_key_exists('tasks', $data) ? $this->__createResponseObject($request, 'set', $data, 'tasks') : false;
            };
        }
        if ($request instanceof ITasksListRequest) {
            // $reqData['method'] = 'GET';
            $reqData['entry'] = '/private/api/v2/{type}/tasks/list';
            $reqData['query'] = $data;
            $reqData['response'] = function($data) use ($request) {
                return array_key_exists('tasks', $data) ? $this->__createResponseObject($request, 'list', $data, 'tasks') : false;
            };
        }
        if ($request instanceof IContactsSetRequest) {
            $reqData['method'] = 'POST';
            $reqData['entry'] = '/private/api/v2/{type}/contacts/set';
            $reqData['post'] = json_encode(['request' => ['contacts' => $data]]);
            $reqData['response'] = function($data) use ($request) {
                return array_key_exists('contacts', $data) ? $this->__createResponseObject($request, 'set', $data, 'contacts') : false;
            };
        }
        if ($request instanceof ILeadsSetRequest) {
            $reqData['method'] = 'POST';
            $reqData['entry'] = '/private/api/v2/{type}/leads/set';
            $reqData['post'] = json_encode(['request' => ['leads' => $data]]);
            $reqData['response'] = function($data) use ($request) {
                return array_key_exists('leads', $data) ? $this->__createResponseObject($request, 'set', $data, 'leads') : false;
            };
        }
        if ($request instanceof ICompaniesSetRequest) {
            $reqData['method'] = 'POST';
            $reqData['entry'] = '/private/api/v2/{type}/company/set';
            $reqData['post'] = json_encode(['request' => ['contacts' => $data]]);
            $reqData['response'] = function($data) use ($request) {
                return array_key_exists('contacts', $data) ? $this->__createResponseObject($request, 'set', $data, 'contacts') : false;
            };
        }
        if ($request instanceof INotesSetRequest) {
            $reqData['method'] = 'POST';
            $reqData['entry'] = '/private/api/v2/{type}/notes/set';
            $reqData['post'] = json_encode(['request' => ['notes' => $data]]);
            $reqData['response'] = function($data) use ($request) {
                return array_key_exists('notes', $data) ? $this->__createResponseObject($request, 'set', $data, 'notes') : false;
            };
        }
        return $reqData;
    }

    protected function getSessionId()
    {
        $sessionId = implode('', array_map('md5', [
            $this->key, $this->login, $this->apiUri
        ]));
        return md5($sessionId);
    }

    protected function updateServerTime($time)
    {
        $tsRemote = ($time instanceof \DateTime) ? $time->getTimestamp() : (int) $time;
        $tsLocal = microtime(true);
        $diff = $tsRemote - $tsLocal;
        if (abs($diff) <= 24*60*60) {
            $this->diffServerTime = $diff;
        } else {
            $this->diffServerTime = 0;
        }
    }

    protected function loadSession($unique)
    {
        if (isset(self::$sessions[$unique])) {
            $session = self::$sessions[$unique];
            if ($this->debug >= 3) {
            	var_dump('session load', $session);
            }
            $this->cookies = $session['cookies'];
            $this->expired = $session['expired'];
            $this->isAuth = $session['isAuth'];
            $this->key = $session['key'];
            $this->lastRequestTime = $session['lastRequestTime'];
            $this->diffServerTime = $session['diffServerTime'];
            $this->login = $session['login'];
        }
    }

    protected function saveSession($unique)
    {
        $session = [];
        $session['cookies'] = $this->cookies;
        $session['expired'] = $this->expired;
        $session['isAuth'] = $this->isAuth;
        $session['key'] = $this->key;
        $session['lastRequestTime'] = $this->lastRequestTime;
        $session['diffServerTime'] = $this->diffServerTime;
        $session['login'] = $this->login;
        self::$sessions[$unique] = $session;
        if ($this->debug >= 3) {
        	var_dump('session save', $session);
        }
    }

    private function sendRequest($query, $method = 'GET', $data = null, $headers = null)
    {
        if ($this->debug >= 2) {
            echo '-> QUERY: ' . $query . PHP_EOL;
            echo '-> POST: ' . (is_string($data) ? $data : var_export($data, 1)) . PHP_EOL;
        }

        if ($sleep = min(max(0, $this->lastRequestTime + $this->diffServerTime - microtime(true) + 1000000), 5000000)) {
            if ($this->debug >= 3) {
                echo 'sleep: ' . $sleep .' (diff=' . $this->diffServerTime . ')' . PHP_EOL;
            }
            usleep($sleep);
        }
        $h = $this->getHttp();
        $h->setCookies($this->cookies);
        $success = $h->query($query, $method, $data, $headers);
        $this->lastRequestTime = microtime(true);
        if (!$success) {
            if ($h->hasErrors()){
                $errors = $h->getErrors();
                throw new \Exception('Network errors: ' . implode('; ', $errors));
            }
            throw new \Exception('Unknown network error');
            //return false;
        }
        $this->cookies = $h->getCookies();
        $statusCode = $h->getResponseCode();


        $contentType = $h->getResponseContentType();
        $body = $h->getResponseBody();

        if ($this->debug >= 2) {
            echo '<- RESPONSE: ' . $body . PHP_EOL;
        }

        $result = [];
        switch ($statusCode) {
            case 200:
                if (strpos($contentType, 'application/json') !== false) {
                    $result['data'] = json_decode($body, true);
                    if (empty($result['data'])) {
                        throw new \Exception('Service sent not valid JSON');
                    }
                } elseif (strpos($contentType, 'xml') !== false) {
                    //$result['data'] = Xml::createArray($body);
                    throw new \Exception('Not implemented');
                } else {
                    throw new \Exception('Service sent unknown content-type');
                }
                break;
            case 204:
                //no content
                return false;
                break;
            default:
                if (isset(self::$_http_messages[$statusCode])) {
                    $msg = self::$_http_messages[$statusCode];
                    $result['data'] = json_decode($body, true);
                    if (!empty($result['data']['response']['error'])) {
                        $msg .= ' - ' . $result['data']['response']['error'];
                    }
                    throw new \Exception('Service sent http code: ' . $statusCode . ', ' . $msg);
                }
                throw new \Exception('Service sent unknown http code: ' . $statusCode);
        }
        return $result;
    }

    private function buildQuery($location, $params = null)
    {
        $url = rtrim($this->apiUri, '/');
        $location = str_replace('{type}', 'json', $location);
        $url .= '/' . ltrim($location, '/');
        if (!empty($params)) {
            $url = $url . '?' . http_build_query($params);
        }
        return $url;
    }

    /**
     * @param IRequest $request
     * @return bool|IResponse
     */
    public function send(IRequest $request)
    {
        $data = $this->buildRequest($request);

        $sessionId = $this->getSessionId();
        $this->loadSession($sessionId);
        $this->auth();
        $url = $this->buildQuery($data['entry'], $data['query']);
        $result = $this->sendRequest($url, $data['method'], $data['post'], $data['headers']);
        $this->saveSession($sessionId);

        //var_export($result['data']['response']);

        if (isset($result['data']['response'])) {
            if (is_callable($data['response'])) {
                return $data['response']($result['data']['response']);
            } else {
                throw new \Exception('No callback for response');
            }
        }

        return false;
    }

    public function close($sessionId = null)
    {
        $current = $this->getSessionId();
        $sessionId = $sessionId ? $sessionId : $current;
        if ($sessionId == $current) {
            $this->isAuth = false;
            $this->cookies = [];
        }
        if (isset(self::$sessions[$sessionId]))
            unset(self::$sessions[$sessionId]);
    }
} 
<?php
/**
 * contacts.php
 *
 * @author: chazer
 * @created: 25.09.14 13:39
 */

use chazer\amocrmlib\classes\fields\CustomField;
use chazer\amocrmlib\classes\contacts\ListAddItem;
use chazer\amocrmlib\classes\contacts\ListUpdateItem;
use chazer\amocrmlib\classes\contacts\requests\ContactsSetRequest;
use chazer\amocrmlib\classes\Schemes;
use chazer\amocrmlib\classes\Types;

Schemes::addScheme(
    ListAddItem::getSchemeName(),
    [
        //Имя контакта
        'name' => [
            'type' => Types::String,
            'require' => true,
        ],
        // Уникальный идентификатор записи в клиентской программе (не обязательный параметр)
        'request_id' => [
            'type' => Types::Number,
        ],
        // Дата создания этого контакта (не обязательный параметр)
        'date_create' => [
            'type' => Types::Timestamp,
        ],
        // Дата последнего изменения этого контакта (не обязательный параметр)
        'last_modified' => [
            'type' => Types::Timestamp,
        ],
        // Уникальный идентификатор ответственного пользователя(пользователи см. Информация аккаунта -
        // https://developers.amocrm.ru/rest_api/accounts_current.php )
        'responsible_user_id' => [
            'type' => Types::Number,
        ],
        // Список связанных сделок
        // ID сделки
        'linked_leads_id' => [
            'type' => Types::ValuesList(Types::Number),
        ],
        // Имя компании
        'company_name' => [
            'type' => Types::String,
        ],
        // Дополнительные поля контакта
        'custom_fields' => [
            'type' => Types::ObjectsList(CustomField::getSchemeName())
        ],
        // Названия тегов через запятую
        'tags' => [
            'type' => Types::String,
        ]
    ]
);

Schemes::addScheme(
    ListUpdateItem::getSchemeName(),
    array_merge(
        Schemes::getScheme(ListAddItem::getSchemeName()),
        [
            // Уникальный идентификатор контакта, который указывается с целью его обновления
            'id' => [
                'require' => true,
                'type' => Types::Number,
            ],
            // Дата последнего изменения данной сущности, если параметр не указан, или он меньше чем имеющийся в БД,
            // то обновление не произойдет и в ответ придет информация из Базы Данных amoCRM
            // (Является обязательным параметром)
            'last_modified' => [
                'require' => true,
                'type' => Types::Timestamp
            ],
        ]
    )
);

Schemes::addScheme(
    ContactsSetRequest::getSchemeName(),
    [
        // Список добавляемых контактов
        'add' => [
            'type' => Types::ObjectsList(ListAddItem::getClassName())
        ],
        // Обновление существующего контакта
        'update' => [
            'type' => Types::ObjectsList(ListUpdateItem::getClassName())
        ],
    ]
);

<?php
/**
 * tasks.php
 *
 * @author: chazer
 * @created: 23.09.14 22:00
 */

use chazer\amocrmlib\api\types\TaskElementTypeNames;
use chazer\amocrmlib\api\types\TaskElementTypes;
use chazer\amocrmlib\classes\Schemes;
use chazer\amocrmlib\classes\tasks\ListAddItem;
use chazer\amocrmlib\classes\tasks\ListUpdateItem;
use chazer\amocrmlib\classes\tasks\requests\TasksListRequest;
use chazer\amocrmlib\classes\tasks\requests\TasksSetRequest;
use chazer\amocrmlib\classes\Types;

Schemes::addScheme(
    ListAddItem::getSchemeName(),
    [
        // Уникальный идентификатор контакта или сделки (сделка или контакт указывается в element_type)
        'element_id' => [
            'require' => true,
            'type' => Types::Number,
        ],
        // Тип привязываемого елемента (1 - контакт, 2 - сделка)
        'element_type' => [
            'require' => true,
            'type' => Types::Enum([TaskElementTypes::CONTACT, TaskElementTypes::LEAD]),
        ],
        // Дата создания данной задачи (не обязательный параметр)
        'date_create' => [
            'type' => Types::Timestamp,
        ],
        // Дата последнего изменения данной задачи (не обязательный параметр)
        'last_modified' => [
            'type' => Types::Timestamp,
        ],
        // Внешний идентификатор записи (не обязательный параметр)(Информация о request_id нигде не сохраняется,
        // лишь передается обратно в ответе)
        'request_id' => [
            'type' => Types::Number,
        ],
        // Тип задачи (типы задач см. Информация аккаунта -
        // https://developers.amocrm.ru/rest_api/accounts_current.php )
        'task_type' => [
            'require' => true,
            'type' => Types::Number,
        ],
        // Текст задачи
        'text' => [
            'require' => true,
            'type' => Types::String,
        ],
        // Уникальный идентификатор ответственного пользователя(пользователи см. Информация аккаунта -
        // https://developers.amocrm.ru/rest_api/accounts_current.php )
        'responsible_user_id' => [
            'type' => Types::Number,
        ],
        // Дата до которой необходимо завершить задачу. Если указано время 23:59, то в интерфейсах системы вместо
        // времени будет отображаться "Весь день".
        'complete_till' => [
            'require' => true,
            'type' => Types::Timestamp,
        ]
    ]
);

Schemes::addScheme(
    ListUpdateItem::getSchemeName(),
    Schemes::merge(
        Schemes::getScheme(ListAddItem::getSchemeName()),
        [
            // Уникальный идентификатор обновляемой задачи
            'id' => [
                'require' => true,
                'type' => Types::Number,
            ],
            'element_id' => [
                'require' => false,
            ],
            'element_type' => [
                'require' => false,
            ],
            'task_type' => [
                'require' => false,
            ],
            'text' => [
                'require' => false,
            ],
            'complete_till' => [
                'require' => false,
            ],
            // Дата последнего изменения данной сущности, если параметр не указан, или он меньше чем имеющийся в БД,
            // то обновление не произойдет и в ответ придет информация из Базы Данных amoCRM
            // (Является обязательным параметром)
            'last_modified' => [
                'require' => true,
                'type' => Types::Timestamp
            ],
        ]
    )
);

Schemes::addScheme(
    TasksSetRequest::getSchemeName(),
    [
        // Список добавляемых задач
        'add' => [
            'type' => Types::ObjectsList(ListAddItem::getClassName())
        ],
        // Изменение существующих задач
        'update' => [
            'type' => Types::ObjectsList(ListUpdateItem::getClassName())
        ],
    ]
);

Schemes::addScheme(
    TasksListRequest::getSchemeName(),
    [
        // Список добавляемых задач
        'if-modified-since' => [
            'type' => Types::Timestamp
        ],
        // Получение данных только для контакта или сделки
        // при отсутствии этого параметра будут полученны все данные, в том числе не прикрепленные к объектам
        'type' => [
            'type' => Types::Enum([TaskElementTypeNames::CONTACT, TaskElementTypeNames::LEAD])
        ],
        // Кол-во выбираемых строк (системное ограничение 500)
        'limit_rows' => [
            'type' => Types::Number(1, 500)
        ],
        // Оффсет выборки (с какой строки выбирать) Работает, только при условии, что limit_rows тоже указан
        'limit_offset' => [
            'type' => Types::Number(0)
        ],
        // Выбрать элемент с заданным ID (Если указан этот параметр, все остальные игнорируются)
        // (Можно передавать в виде массива состоящий из нескольких ID)
        'id' => [
            'type' => [Types::Number, Types::ValuesList(Types::Number)],
        ],
        // Дополнительный фильтр поиска, по ответственному пользователю
        // (Можно передавать в виде массива)
        'responsible_user_id' => [
            'type' => [Types::Number, Types::ValuesList(Types::Number)],
        ]
    ]
);

<?php
/**
 * notes.php
 *
 * @author: aitribunsky
 * @created: 13.02.15 20:56
 */
use chazer\amocrmlib\classes\notes\ListAddItem;
use chazer\amocrmlib\classes\notes\ListUpdateItem;
use chazer\amocrmlib\classes\Schemes;
use chazer\amocrmlib\classes\Types;
use chazer\amocrmlib\classes\notes\requests\NotesSetRequest;

Schemes::addScheme(ListAddItem::getSchemeName(), [
    'element_id' => [
        'type' => Types::String,
        'require' => true
    ],
    'element_type' => [
        'type' => Types::String,
        'require' => true
    ],
    'note_type' => [
        'type' => Types::String,
        'require' => true
    ],
    'date_create' => [
        'type' => Types::Timestamp
    ],
    'request_id' => [
        'type' => Types::Number
    ],
    'last_modified' => [
        'type' => Types::Timestamp
    ],
    'text' => [
        'type' => Types::String,
        'require' => true
    ],
    'responsible_user_id' => [
        'type' => Types::Number
    ]
]);

Schemes::addScheme(ListUpdateItem::getSchemeName(), Schemes::merge(Schemes::getScheme(ListAddItem::getSchemeName()), [
    'id' => [
        'require' => true,
        'type' => Types::Number
    ],
    'last_modified' => [
        'require' => true,
        'type' => Types::Timestamp
    ]
]));

Schemes::addScheme(NotesSetRequest::getSchemeName(), [
    // Список добавляемых событий
    'add' => [
        'type' => Types::ObjectsList(ListAddItem::getClassName())
    ],
    // Обновление существующего события
    'update' => [
        'type' => Types::ObjectsList(ListUpdateItem::getClassName())
    ]
]);

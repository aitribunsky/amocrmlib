<?php
/**
 * IListAddItem.php
 *
 * @author: chazer
 * @created: 26.09.14 18:38
 */

namespace chazer\amocrmlib\api\interfaces\leads;

use chazer\amocrmlib\api\interfaces\fields\ICustomField;
use chazer\amocrmlib\api\interfaces\ICommonListAddItem;
use DateTime;

interface IListAddItem extends ICommonListAddItem
{
    /**
     * Название сделки
     * @return string
     */
    public function getName();

    /**
     * Дата создания текущей сделки
     * @return null|DateTime
     */
    public function getDateCreate();

    /**
     * Дата изменения текущей сделки
     * @return null|DateTime
     */
    public function getLastModified();

    /**
     * Статус сделки
     * @return int
     */
    public function getStatusId();

    /**
     * Бюджет сделки
     * @return null|int
     */
    public function getPrice();

    /**
     * Уникальный идентификатор ответственного пользователя
     * @return null|int
     */
    public function getResponsibleUserId();

    /**
     * Уникальный идентификатор записи в клиентской программе (не обязательный параметр)
     * (Информация о request_id нигде не сохраняется)
     * @return null|int
     */
    public function getRequestId();

    /**
     * Добавить доп. поле
     * @param ICustomField $item
     */
    public function addCustomField(ICustomField $item);

    /**
     * Дополнительные поля
     * @return null|array
     */
    public function getCustomFields();

    /**
     * Названия тегов через запятую
     * @return null|string
     */
    public function getTags();

    /**
     * ID связываемой компании
     * @return mixed
     */
    public function getLinkedCompanyId();
}

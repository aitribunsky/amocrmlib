<?php
/**
 * ISetRequest.php
 *
 * @author: chazer
 * @created: 30.09.14 14:50
 */

namespace chazer\amocrmlib\api\interfaces;

interface ISetRequest extends IRequest
{
    /**
     * @return ICommonListAddItem[]
     */
    public function getAdd();

    public function getUpdate();
} 
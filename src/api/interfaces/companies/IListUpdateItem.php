<?php
/**
 * IListUpdateItem.php
 *
 * @author: chazer
 * @created: 15.10.14 17:54
 */

namespace chazer\amocrmlib\api\interfaces\companies;

use chazer\amocrmlib\api\interfaces\fields\ICustomField;
use chazer\amocrmlib\api\interfaces\ICommonListUpdateItem;
use DateTime;

interface IListUpdateItem extends ICommonListUpdateItem
{
    /**
     * @return int Уникальный идентификатор компании, который указывается с целью его обновления
     */
    public function getId();

    /**
     * Имя компании
     * @return string
     */
    public function getName();

    /**
     * Уникальный идентификатор записи в клиентской программе
     * @return null|int
     */
    public function getRequestId();

    /**
     * Дата создания этой компании
     * @return null|DateTime
     */
    public function getDateCreate();

    /**
     * Дата последнего изменения данной сущности, если параметр не указан, или он меньше чем имеющийся в БД,
     * то обновление не произойдет и в ответ придет информация из Базы Данных amoCRM
     * @return DateTime
     */
    public function getLastModified();

    /**
     * Уникальный идентификатор ответственного пользователя
     * @return null|int
     */
    public function getResponsibleUserId();

    /**
     * @param $id int linked lead id
     */
    public function linkLead($id);

    /**
     * Список id связанных сделок
     * @return null|array
     */
    public function getLinkedLeadsId();

    /**
     * Добавить доп. поле
     * @param ICustomField $item
     */
    public function addCustomField(ICustomField $item);

    /**
     * Дополнительные поля компании
     * @return null|array
     */
    public function getCustomFields();

    /**
     * Названия тегов через запятую
     * @return null|string
     */
    public function getTags();
} 
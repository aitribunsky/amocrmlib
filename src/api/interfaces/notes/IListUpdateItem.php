<?php
/**
 * IListUpdateItem.php
 *
 * @author: anatolyt
 * @created: 13.02.15 19:48
 */
namespace chazer\amocrmlib\api\interfaces\notes;

use chazer\amocrmlib\api\interfaces\ICommonListUpdateItem;

interface IListUpdateItem extends ICommonListUpdateItem
{

    /**
     *
     * @return int Уникальный идентификатор события
     */
    public function getId();

    /**
     * Уникальный идентификатор контакта или сделки (сделка или контакт указывается в element_type)
     */
    public function getElementId();

    /**
     * Тип привязываемого елемента (контакт или сделка)
     */
    public function getElementType();

    /**
     * Тип события
     * https://developers.amocrm.ru/rest_api/notes_list.php
     */
    public function getNoteType();

    /**
     * Дата создания
     *
     * @return null|\DateTime
     */
    public function getDateCreate();

    /**
     * Уникальный идентификатор записи в клиентской программе (не обязательный параметр)
     * (Информация о request_id нигде не сохраняется)
     *
     * @return null|int
     */
    public function getRequestId();

    /**
     * Дата изменения текущей сделки
     *
     * @return null|\DateTime
     */
    public function getLastModified();

    /**
     * Текст задачи
     */
    public function getText();

    /**
     * Уникальный идентификатор ответственного пользователя
     *
     * @return null|int
     */
    public function getResponsibleUserId();
} 
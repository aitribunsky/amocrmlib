<?php
/**
 * INotesSetRequest.php
 *
 * @author: aitribunsky
 * @created: 13.02.14 19:37
 */
namespace chazer\amocrmlib\api\interfaces\notes\requests;

use chazer\amocrmlib\api\interfaces\ISetRequest;
use chazer\amocrmlib\api\interfaces\notes\IListAddItem;
use chazer\amocrmlib\api\interfaces\notes\IListUpdateItem;

/**
 * Interface INotesSetRequest
 * 
 * @method IListAddItem[] getAdd
 * @method IListUpdateItem[] getUpdate
 * @package chazer\amocrmlib\api\interfaces\notes\requests
 */
interface INotesSetRequest extends ISetRequest
{

    public function add(IListAddItem $item);

    public function update(IListUpdateItem $item);
}
<?php
/**
 * ICustomField.php
 *
 * @author: chazer
 * @created: 25.09.14 14:58
 */

namespace chazer\amocrmlib\api\interfaces\fields;

interface ICustomField
{
    /**
     * @return int Уникальный идентификатор заполняемого дополнительного поля (см. Информация аккаунта -
     * https://developers.amocrm.ru/rest_api/accounts_current.php)
     */
    public function getId();

    /**
     * @param ICustomFieldValue $value
     */
    public function addValue(ICustomFieldValue $item);

    /**
     * @return array Значения доп. поля
     */
    public function getValues();
} 
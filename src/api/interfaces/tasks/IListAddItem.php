<?php
/**
 * TasksRequestAddItem.php
 *
 * @author: chazer
 * @created: 23.09.14 12:20
 */

namespace chazer\amocrmlib\api\interfaces\tasks;

use chazer\amocrmlib\api\interfaces\ICommonListAddItem;

interface IListAddItem extends ICommonListAddItem
{
    public function getElementId();

    public function getElementType();

    public function getDateCreate();

    public function getLastModified();

    public function getRequestId();

    public function getTaskType();

    public function getText();

    public function getResponsibleUserId();

    public function getCompleteTill();
}
<?php
/**
 * ITasksSetRequest.php
 *
 * @author: chazer
 * @created: 23.09.14 13:46
 */

namespace chazer\amocrmlib\api\interfaces\tasks\requests;

use chazer\amocrmlib\api\interfaces\ISetRequest;
use chazer\amocrmlib\api\interfaces\tasks\IListAddItem;
use chazer\amocrmlib\api\interfaces\tasks\IListUpdateItem;

/**
 * Interface ITasksSetRequest
 * @method IListAddItem[] getAdd
 * @method IListUpdateItem[] getUpdate
 * @package chazer\amocrmlib\api\interfaces\tasks\requests
 */
interface ITasksSetRequest extends ISetRequest
{
    public function add(IListAddItem $item);

    public function update(IListUpdateItem $item);
}
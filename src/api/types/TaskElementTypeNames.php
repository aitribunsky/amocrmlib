<?php
/**
 * TaskElementTypeName.php
 *
 * @author: chazer
 * @created: 24.09.14 13:02
 */

namespace chazer\amocrmlib\api\types;

class TaskElementTypeNames extends TaskElementTypes
{
    const CONTACT = 'contact';
    const LEAD = 'lead';

    private static $names = [
        TaskElementTypes::CONTACT => self::CONTACT,
        TaskElementTypes::LEAD => self::LEAD,
    ];

    public static function toInt($string)
    {
        return array_search($string, self::$names);
    }

    public static function toString($int)
    {
        return isset(self::$names[$int]) ? self::$names[$int] : false;
    }
}

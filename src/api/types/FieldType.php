<?php
/**
 * FieldType.php
 *
 * @author: chazer
 * @created: 25.09.14 16:22
 */

namespace chazer\amocrmlib\api\types;

class FieldType
{
    const TEXT = 1;
    const NUMERIC = 2;
    const CHECKBOX = 3;
    const SELECT = 4;
    const MULTISELECT = 5;
    const DATE = 6;
    const URL = 7;
    const MULTITEXT = 8;
    const TEXTAREA = 9;
    const RADIOBUTTON = 10;
} 
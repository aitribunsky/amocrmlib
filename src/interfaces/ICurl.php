<?php
/**
 * ICurl.php
 *
 * @author: chazer
 * @created: 23.09.14 14:12
 */

namespace chazer\amocrmlib\interfaces;

interface ICurl
{
    /**
     * Initialize a cURL session
     * @link http://php.net/manual/en/function.curl-init.php
     * @param string $url [optional]
     */
    public function curlInit($url = null);

    /**
     * @param int $option
     * @param mixed $value
     *
     * @return bool true on success or false on failure.
     */
    public function curlSetOpt($option, $value);

    /**
     * Get information regarding a specific transfer
     * @param int $opt [optional]
     * @return mixed
     */
    public function curlGetInfo($opt = null);

    /**
     * @return mixed true on success or false on failure. However, if the CURLOPT_RETURNTRANSFER option is set,
     * it will return the result on success, false on failure.
     */
    public function curlExec();

    /**
     * Return a string containing the last error for the current session
     * @return string
     */
    public function curlError();

    /**
     * Return the last error number
     * @return int
     */
    public function curlErrNo();

    /**
     * Close a cURL session
     */
    public function curlClose();
} 
<?php
/**
 * SplEnum class
 *
 * @author: Jacek Skirzyński
 * @url: http://blog.skirzynski.eu/2013/10/typ-wyliczeniowy-splenum-i-alternatywy/
 */

if (!class_exists('SplEnum')) {
    class SplEnum
    {
        /**
         * Value of object
         * @var mixed
         */
        private $value;

        /**
         * Name of const with default value
         * @var string
         */
        private $defaultName = '__default';

        /**
         * Reflection object
         * @var ReflectionClass
         */
        private $reflection;

        /**
         * Create object
         * @param mixed $initialValue
         * @throws UnexpectedValueException
         */
        public function __construct($initialValue = null)
        {
            if ($initialValue && in_array($initialValue, $this->getConstList())) {
                $this->value = $initialValue;
            } elseif (!$initialValue && $this->getDefault()) {
                $this->value = $this->getDefault();
            } else {
                throw new UnexpectedValueException('Value not a const in enum '. get_class($this));
            }
        }

        /**
         * Get array of class consts
         * @param boolean $includeDefault
         * @return array
         */
        public function getConstList($includeDefault = false)
        {
            $tmp = $this->getReflection()->getConstants();
            if (!$includeDefault && array_key_exists($this->defaultName, $tmp)) {
                unset($tmp[$this->defaultName]);
            }
            return $tmp;
        }

        /**
         * Get value of object
         * @return mixed
         */
        public function getValue()
        {
            return $this->value;
        }

        public function __toString()
        {
            return (string) $this->getValue();
        }

        /**
         * Get default value from class
         * @return mixed
         */
        protected function getDefault()
        {
            return $this->getReflection()->getConstant($this->defaultName);
        }

        /**
         * Get instance of class reflection
         * @return ReflectionClass
         */
        protected function getReflection()
        {
            if (!$this->reflection) {
                $this->reflection = new ReflectionClass($this);
            }

            return $this->reflection;
        }
    }
}